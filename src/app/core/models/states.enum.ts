// States of the http requests to control the loaders
export enum HttpReqStatus {
    NONE = 'NONE',
    SENT = 'SENT',
    RESPONSE = 'RESPONSE'
};

// Actions to change the state of the postList subject
export enum ActionType { ADD, UPDATE, DELETE };