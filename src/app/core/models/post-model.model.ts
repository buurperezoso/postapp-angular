import { Comment } from './comment.model';

export class Post {
    id: string;
    title: string;
    description: string;
    category: string;
    imgRoute: string;
    user: string;
    creationDate: string;
    comments: Comment[];
}