import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

// Models
import { Post } from '../models/post-model.model';

// Services
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService,
  ) { }

  async addPost(form: Post): Promise<Post> {
    try {
      return await this.http.post<Post>(`${environment.apiURL}/create-post`, form).toPromise();
    } catch (error) {
      this.errorHandlerService.handleError(error, () => {
        this.addPost(form);
      });
    }
  }

  async editPost(form: Post): Promise<Post> {
    try {
      return await this.http.post<Post>(`${environment.apiURL}/post/${form.id}/update`, form).toPromise();
    } catch (error) {
      this.errorHandlerService.handleError(error, () => {
        this.editPost(form);
      });
    }
  }

  async deletePost(form: Post): Promise<string> {
    try {
      return await this.http.delete<string>(`${environment.apiURL}/post/${form.id}/delete`).toPromise();
    } catch (error) {
      this.errorHandlerService.handleError(error, () => {
        this.deletePost(form);
      });
    }
  }

  async addComment(form: Comment, postID: string): Promise<Post> {
    try {
      return await this.http.post<Post>(`${environment.apiURL}/post/${postID}/comment`, form).toPromise();
    } catch (error) {
      this.errorHandlerService.handleError(error, () => {
        this.addComment(form, postID);
      });
    }
  }

}
