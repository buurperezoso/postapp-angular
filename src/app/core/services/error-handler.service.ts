import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs';

// Material dialog
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NoopScrollStrategy } from '@angular/cdk/overlay';

// Components
import { ErrorHandlerModalComponent } from 'src/app/shared/components/error-handler-modal/error-handler-modal.component';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(
    private dialog: MatDialog,
  ) { }

  handleError(error: any, functionOnClose: () => void) {

    const dialogConfig = new MatDialogConfig();
    dialogConfig.scrollStrategy = new NoopScrollStrategy();
    dialogConfig.disableClose = true;

    const dialogRef = this.dialog.open(ErrorHandlerModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        functionOnClose();
      }
      console.error(error.error);
    });

  }

}
