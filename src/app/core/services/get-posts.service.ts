import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

// Models
import { Post } from '../models/post-model.model';
import { HttpReqStatus, ActionType } from '../models/states.enum';

// Services
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class GetPostsService {

  private postsList$ = new BehaviorSubject<{ status: HttpReqStatus, postsList: Post[] }>({ status: HttpReqStatus.SENT, postsList: [] });

  constructor(
    private http: HttpClient,
    private errorHandlerService: ErrorHandlerService,
  ) { }

  requestPostsList(): void {

    this.http.get<Post[]>(`${environment.apiURL}/posts`).
      subscribe(
        (response) => {
          const actualArray = this.postsList$.value.postsList;
          if (JSON.stringify(actualArray) !== JSON.stringify(response)) {
            this.postsList$.next({ status: HttpReqStatus.RESPONSE, postsList: response });
          }
        },
        (error) => {
          this.errorHandlerService.handleError(error, () => {
            this.requestPostsList();
          });
        }
      );

  }

  get getPostsList(): BehaviorSubject<{ status: HttpReqStatus, postsList: Post[] }> {
    return this.postsList$;
  }

  updatePostList(type: ActionType, post: Post) {

    const array = this.postsList$.value.postsList;

    switch (type) {
      case ActionType.ADD:
        this.postsList$.next({ status: HttpReqStatus.RESPONSE, postsList: [post, ...array] });
        break;
      case ActionType.UPDATE:
        const newArray = array.map((item) => {
          if (item.id === post.id) {
            item = post;
          }
          return item;
        });
        this.postsList$.next({ status: HttpReqStatus.RESPONSE, postsList: newArray });
        break;
      case ActionType.DELETE:
        const deleteArray = array.filter((item) => item.id !== post.id);
        this.postsList$.next({ status: HttpReqStatus.RESPONSE, postsList: deleteArray });
        break;
    }
  }

}
