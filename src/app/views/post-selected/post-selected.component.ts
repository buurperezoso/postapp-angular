import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

// Models
import { Post } from 'src/app/core/models/post-model.model';

// Services
import { GetPostsService } from 'src/app/core/services/get-posts.service';

@Component({
  selector: 'app-post-selected',
  templateUrl: './post-selected.component.html',
  styleUrls: ['./post-selected.component.scss']
})
export class PostSelectedComponent implements OnInit, OnDestroy {

  // Subscriptions
  subPostList: Subscription;

  postID: string; //Gets from params
  postSelected: Post; // Saves here the value of the post selected

  constructor(
    private activatedRoute: ActivatedRoute,
    private getPostService: GetPostsService,
    private router: Router,
    private titleService: Title
  ) {
    this.postID = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    window.scrollTo(0, 0);

    this.getPostsList();
  }

  ngOnDestroy(): void {
    if (this.subPostList) {
      this.subPostList.unsubscribe();
    }
  }

  getPostsList(): void {
    this.subPostList = this.getPostService.getPostsList.subscribe(
      (response) => {
        if (response.postsList.length > 0) {
          this.getPost(response.postsList);
        }
      }
    )
  }

  getPost(postsList: Post[]) {
    this.postSelected = postsList.find((post) => post.id === this.postID);

    if (postsList.length > 0 && !this.postSelected) {
      this.router.navigate(['/']);
    } else {
      this.titleService.setTitle(this.postSelected.title);
    }
  }

}
