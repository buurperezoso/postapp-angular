import { Component, Input, OnInit } from '@angular/core';

// Models
import { Comment } from 'src/app/core/models/comment.model';

@Component({
  selector: 'app-comment',
  template: `
    <div class="row background-coment mt-4" *ngIf="comment">

      <div class="col-12 mt-2">

        <fa-icon [icon]="['fas', 'user-circle']"
                class="icon-comment"></fa-icon>
        <span class="user-comment ml-2 align-top">{{comment.user}}</span>

      </div>

      <div class="col-11 ml-auto mr-auto mt-1">

        <p class="text-justify">
            {{comment.comment}}
        </p>

      </div>

    </div>
  `,
  styles: [`
    .background-coment {
      background-color: white;
      border-radius: 0.5rem;
    }

    .icon-comment {
        font-size: 1.8rem;
    }

    .user-comment {
        font-size: 1.4rem;
    }
  `]
})
export class CommentComponent implements OnInit {

  @Input() comment: Comment;

  constructor() { }

  ngOnInit(): void {
  }

}
