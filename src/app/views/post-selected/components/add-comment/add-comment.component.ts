import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

// Models
import { HttpReqStatus, ActionType } from 'src/app/core/models/states.enum';

// Services
import { GetPostsService } from 'src/app/core/services/get-posts.service';
import { HttpRequestService } from 'src/app/core/services/http-request.service';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.scss']
})
export class AddCommentComponent implements OnInit {

  @Input() postID: string;

  commentForm: FormGroup;
  httpStatus: HttpReqStatus = HttpReqStatus.NONE; // Controls the loader in button
  user = localStorage.getItem('user');

  constructor(
    private formBuilder: FormBuilder,
    private httpRequestService: HttpRequestService,
    private getPostService: GetPostsService
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.commentForm = this.formBuilder.group({
      comment: ['', Validators.compose([
        Validators.required
      ])],
      user: [this.user]
    })
  }

  async saveComment(formToClean: NgForm): Promise<void> {

    if (!this.commentForm.valid) {
      return;
    }

    let post;

    this.httpStatus = HttpReqStatus.SENT;
    post = await this.httpRequestService.addComment(this.commentForm.value, this.postID);

    this.httpStatus = HttpReqStatus.RESPONSE;

    if (post) {
      formToClean.reset();
      this.commentForm.controls.user.setValue(this.user);

      this.getPostService.updatePostList(ActionType.UPDATE, post);
    }

  }

}
