import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-filter-buttons',
  templateUrl: './filter-buttons.component.html',
  styleUrls: ['./filter-buttons.component.scss']
})
export class FilterButtonsComponent implements OnInit {

  @Input() category;
  @Output() categoryChange = new EventEmitter<string>();

  buttonsList = environment.category;

  constructor() { }

  ngOnInit(): void {
  }

  onClickCategory(categoryClicked: string): void {
    this.categoryChange.emit(categoryClicked);
  }

}
