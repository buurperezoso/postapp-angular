import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-button-add',
  template: `
    <button 
      class="btn btn-rounded-coral custom-shadow"
      type="button"
      mat-button
    >
      <fa-icon [icon]="['fas', 'pen']"></fa-icon>
    </button>
  `,
  styles: [`
    .btn-rounded-coral {
      color: #fff;
      background-color: coral;
      border-color: coral;
      transition: .4s;
      height: 4rem;
      width: 4rem;
      border-radius: 50%;
      font-size: 1.2rem;
    }

    .btn-rounded-coral:hover {
        color: #fff;
        background-color: rgb(212, 109, 71);
        border-color: rgb(212, 109, 71);
        transform: scale(1.10);
    }

    .btn-rounded-coral:focus, .btn-rounded-coral.focus {
        color: #fff;
        background-color: rgb(212, 109, 71);
        border-color: rgb(212, 109, 71);
        box-shadow: 0 0.4rem 0.3rem #9E9E9E!important;
    }
  `]
})
export class ButtonAddComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
