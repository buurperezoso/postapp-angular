import { Component, Input, OnInit } from '@angular/core';

// Material dialog
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NoopScrollStrategy } from '@angular/cdk/overlay';

// Models
import { Post } from 'src/app/core/models/post-model.model';

// Services
import { GetPostsService } from 'src/app/core/services/get-posts.service';

// Components
import { AddPostModalComponent } from 'src/app/shared/components/add-post-modal/add-post-modal.component';
import { DeletePostModalComponent } from 'src/app/shared/components/delete-post-modal/delete-post-modal.component';

@Component({
  selector: 'app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {

  @Input() post: Post;

  constructor(
    private getPostService: GetPostsService,
    private dialog: MatDialog,
  ) { }

  ngOnInit(): void {
  }

  editPost(post: Post, event: Event): void {
    event.preventDefault();
    event.stopPropagation();

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.data = post;
    dialogConfig.scrollStrategy = new NoopScrollStrategy();

    const dialogRef = this.dialog.open(AddPostModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getPostService.updatePostList(result.type, result.post);
      }
    });

  }

  deletePost(post: Post, event: Event) {
    event.preventDefault();
    event.stopPropagation();

    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.data = post;
    dialogConfig.scrollStrategy = new NoopScrollStrategy();

    const dialogRef = this.dialog.open(DeletePostModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getPostService.updatePostList(result.type, result.post);
      }
    });

  }

}
