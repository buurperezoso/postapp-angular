import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

// Material dialog
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NoopScrollStrategy } from '@angular/cdk/overlay';

// Models
import { Post } from 'src/app/core/models/post-model.model';
import { HttpReqStatus } from 'src/app/core/models/states.enum';

// Services
import { GetPostsService } from 'src/app/core/services/get-posts.service';

// Components
import { AddPostModalComponent } from 'src/app/shared/components/add-post-modal/add-post-modal.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {

  // Subscriptions
  subPostList: Subscription;
  subDialogResponse: Subscription;

  postsList: Post[]; // Save array of post when is received from postList Subject
  filteredPostsList: Post[];
  category = 'ALL';
  httpReqStatus: HttpReqStatus = HttpReqStatus.SENT; // Controls the loader depending on the response of the postList Subject

  constructor(
    private getPostService: GetPostsService,
    private dialog: MatDialog,
    private titleService: Title
  ) {
    titleService.setTitle('Discovering the World');
  }

  ngOnInit(): void {
    this.getPostList();
  }

  ngOnDestroy(): void {
    if (this.subPostList) {
      this.subPostList.unsubscribe();
    }

    if (this.subDialogResponse) {
      this.subDialogResponse.unsubscribe();
    }
  }

  getPostList(): void {
    this.subPostList = this.getPostService.getPostsList.subscribe(
      (response) => {
        this.httpReqStatus = response.status;
        if (this.httpReqStatus === HttpReqStatus.RESPONSE) {
          this.postsList = response.postsList;
        }
      }
    )
  }

  openAddPostModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.scrollStrategy = new NoopScrollStrategy();

    const dialogRef = this.dialog.open(AddPostModalComponent, dialogConfig);

    this.subDialogResponse = dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.getPostService.updatePostList(result.type, result.post);
      }
    });
  }

}
