import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Post } from 'src/app/core/models/post-model.model';
import { HttpReqStatus } from 'src/app/core/models/states.enum';
import { GetPostsService } from 'src/app/core/services/get-posts.service';
import { FilterPostsListPipe } from 'src/app/shared/pipes/filter-posts.pipe';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatDialogModule
      ],
      declarations: [HomeComponent, FilterPostsListPipe],
      providers: [
        { provide: GetPostsService, useClass: MockGetPostsService }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

class MockGetPostsService {

  get getPostsList(): Observable<{ status: HttpReqStatus, postsList: Post[] }> {
    return of({ status: HttpReqStatus.RESPONSE, postsList: [] });
  }

}
