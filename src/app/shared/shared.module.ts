import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

// Angular Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// Pipes
import { FilterPostsListPipe } from './pipes/filter-posts.pipe';

// Components
import { LoadingAnimationComponent } from './components/loading-animation/loading-animation.component';
import { AddPostModalComponent } from './components/add-post-modal/add-post-modal.component';
import { DeletePostModalComponent } from './components/delete-post-modal/delete-post-modal.component';
import { ErrorHandlerModalComponent } from './components/error-handler-modal/error-handler-modal.component';
import { LoginModalComponent } from './components/login-modal/login-modal.component';
import { LogoutModalComponent } from './components/logout-modal/logout-modal.component';

@NgModule({
  declarations: [
    LoadingAnimationComponent,
    FilterPostsListPipe,
    AddPostModalComponent,
    DeletePostModalComponent,
    ErrorHandlerModalComponent,
    LoginModalComponent,
    LogoutModalComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule
  ],
  exports: [
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    LoadingAnimationComponent,
    FilterPostsListPipe,
    MatDialogModule,
    AddPostModalComponent,
    DeletePostModalComponent,
    MatIconModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    ErrorHandlerModalComponent,
    LoginModalComponent,
    LogoutModalComponent
  ]
})
export class SharedModule { }
