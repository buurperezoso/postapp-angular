import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';

@Component({
  selector: 'app-error-handler-modal',
  templateUrl: './error-handler-modal.component.html',
  styleUrls: ['./error-handler-modal.component.scss']
})
export class ErrorHandlerModalComponent implements OnInit {

  timerSub: Subscription;

  enableRetry = false;
  timerValue = 6;

  constructor(
    private dialogRef: MatDialogRef<ErrorHandlerModalComponent>,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.startContDown();
  }

  startContDown(): void {
    this.countDown();
  }

  closeAndRetry(): void {
    this.dialogRef.close(true);
  }

  closeAndHome(): void {
    this.dialogRef.close(null);
    this.router.navigate(['/']);
  }

  countDown(): void {
    this.timerSub = timer(0, 1000).subscribe((time) => {
      if (time > 4) {
        this.enableRetry = true;
        this.stopCountDown();
      }

      --this.timerValue;
    });
  }

  stopCountDown() {
    this.timerSub.unsubscribe();
  }

}
