import { Component, OnInit } from '@angular/core';

// Material dialog
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-logout-modal',
  templateUrl: './logout-modal.component.html',
  styleUrls: ['./logout-modal.component.scss']
})
export class LogoutModalComponent implements OnInit {

  user = localStorage.getItem('user');

  constructor(
    private dialogRef: MatDialogRef<LogoutModalComponent>
  ) { }

  ngOnInit(): void {
  }

  logOut() {
    this.dialogRef.close(true);
  }

}
