import { Component, Inject, OnInit } from '@angular/core';

// Material dialog
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

// Models
import { Post } from 'src/app/core/models/post-model.model';
import { HttpReqStatus, ActionType } from 'src/app/core/models/states.enum';

// Services
import { HttpRequestService } from 'src/app/core/services/http-request.service';

@Component({
  selector: 'app-delete-post-modal',
  templateUrl: './delete-post-modal.component.html',
  styleUrls: ['./delete-post-modal.component.scss']
})
export class DeletePostModalComponent implements OnInit {

  deletePostData: Post;
  httpStatus: HttpReqStatus = HttpReqStatus.RESPONSE; // Controls the loader in button depending on the response of the API request

  constructor(
    private httpRequestService: HttpRequestService,
    private dialogRef: MatDialogRef<DeletePostModalComponent>,
    @Inject(MAT_DIALOG_DATA) data
  ) {
    this.deletePostData = data;
  }

  ngOnInit(): void {
  }

  async deletePost(PostData: Post): Promise<void> {
    this.httpStatus = HttpReqStatus.SENT;
    const response = await this.httpRequestService.deletePost(PostData);

    this.httpStatus = HttpReqStatus.RESPONSE;

    if (response) {
      this.dialogRef.close({ type: ActionType.DELETE, post: PostData });
    }

  }

}
