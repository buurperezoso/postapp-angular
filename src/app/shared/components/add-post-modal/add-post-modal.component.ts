import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from 'src/environments/environment';

// Material dialog
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

// Models
import { Post } from 'src/app/core/models/post-model.model';
import { HttpReqStatus, ActionType } from 'src/app/core/models/states.enum';

// Services
import { HttpRequestService } from 'src/app/core/services/http-request.service';

// Validators
import { regexValidator, urlRegex } from 'src/app/shared/validators/validators';

@Component({
  selector: 'app-add-post-modal',
  templateUrl: './add-post-modal.component.html',
  styleUrls: ['./add-post-modal.component.scss']
})
export class AddPostModalComponent implements OnInit {

  postForm: FormGroup;
  categories = environment.category;
  httpStatus: HttpReqStatus = HttpReqStatus.NONE; // Controls the loader in button depending on the response of the API request
  editData: Post;

  constructor(
    private formBuilder: FormBuilder,
    private httpRequestService: HttpRequestService,
    private dialogRef: MatDialogRef<AddPostModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.editData = data;
  }

  ngOnInit(): void {
    this.initForm(this.editData);
  }

  initForm(data?: Post): void {

    if (data) {
      this.formEditPost(data);
    } else {
      this.formAddPost();
    }

  }

  formAddPost() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.compose([
        Validators.required
      ])],
      description: ['', Validators.compose([
        Validators.required
      ])],
      category: ['', Validators.compose([
        Validators.required
      ])],
      imgRoute: ['', Validators.compose([
        Validators.required,
        regexValidator(urlRegex, { noURL: true })
      ])],
    });
  }

  formEditPost(data: Post): void {
    const { id, title, description, category, imgRoute } = data;
    this.postForm = this.formBuilder.group({
      id: [id],
      title: [title, Validators.compose([
        Validators.required
      ])],
      description: [description, Validators.compose([
        Validators.required
      ])],
      category: [category, Validators.compose([
        Validators.required
      ])],
      imgRoute: [imgRoute, Validators.compose([
        Validators.required,
        regexValidator(urlRegex, { noURL: true })
      ])],
    });
  }

  errorImgURL(control: AbstractControl): string {
    let message: string;

    if (control.hasError('noURL')) {
      message = 'Invalid image url.';
    } else {
      message = 'This field is requiered.';
    }

    return message;
  }

  async savePost(form: FormGroup): Promise<void> {

    if (!form.valid) {
      return;
    }

    this.httpStatus = HttpReqStatus.SENT;
    let response, type;

    if (this.editData) {
      response = await this.httpRequestService.editPost(form.value);
      type = ActionType.UPDATE;
    } else {
      response = await this.httpRequestService.addPost(form.value);
      type = ActionType.ADD;
    }

    this.httpStatus = HttpReqStatus.RESPONSE;

    if (response) {
      this.dialogRef.close({ type: type, post: response });
    }

  }

}
