import { Pipe, PipeTransform } from "@angular/core";
import { Post } from 'src/app/core/models/post-model.model';

@Pipe({
    name: 'filterPostsList'
})
export class FilterPostsListPipe implements PipeTransform {

    transform(postsList: Post[], category: string) {

        if (category === 'ALL') {
            return postsList;
        } else {
            return postsList.filter((post) => post.category.toUpperCase() === category);
        }

    }

}