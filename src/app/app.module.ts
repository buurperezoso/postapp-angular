import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Fontawsome
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { HomeComponent } from './views/home/home.component';
import { PostSelectedComponent } from './views/post-selected/post-selected.component';
import { ButtonAddComponent } from './views/home/components/button-add/button-add.component';
import { FilterButtonsComponent } from './views/home/components/filter-buttons/filter-buttons.component';
import { PostCardComponent } from './views/home/components/post-card/post-card.component';
import { CommentComponent } from './views/post-selected/components/comment/comment.component';
import { AddCommentComponent } from './views/post-selected/components/add-comment/add-comment.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PostSelectedComponent,
    ButtonAddComponent,
    FilterButtonsComponent,
    PostCardComponent,
    CommentComponent,
    AddCommentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    FontAwesomeModule
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(_library: FaIconLibrary) {
    _library.addIconPacks(fas, fab, far);
  }
}
