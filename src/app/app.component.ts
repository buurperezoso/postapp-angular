import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

// Material dialog
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NoopScrollStrategy } from '@angular/cdk/overlay';

// Services
import { GetPostsService } from './core/services/get-posts.service';

// Components
import { LoginModalComponent } from './shared/components/login-modal/login-modal.component';
import { LogoutModalComponent } from './shared/components/logout-modal/logout-modal.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  // Subscriptions
  subLogIn: Subscription;
  subLogOut: Subscription;

  constructor(
    private getPostService: GetPostsService,
    private dialog: MatDialog,
    private router: Router,
  ) { }

  ngOnInit(): void {

    this.getPostService.requestPostsList();

  }

  ngOnDestroy(): void {
    if (this.subLogIn) {
      this.subLogIn.unsubscribe();
    }

    if (this.subLogOut) {
      this.subLogOut.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    if (!localStorage.getItem('user')) {
      this.openLoginModal();
    }
  }

  openLoginModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.scrollStrategy = new NoopScrollStrategy();

    const dialogRef = this.dialog.open(LoginModalComponent, dialogConfig);

    this.subLogIn = dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        localStorage.setItem('user', result.user);
      }
    });
  }

  openLogoutModal(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.scrollStrategy = new NoopScrollStrategy();

    const dialogRef = this.dialog.open(LogoutModalComponent, dialogConfig);

    this.subLogOut = dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        localStorage.removeItem('user');
        this.router.navigate(['/']);
        this.openLoginModal();
      }
    });
  }

}
