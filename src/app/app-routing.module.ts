import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Guards
import { AuthGuard } from './core/guards/auth.guard';

// Components
import { HomeComponent } from './views/home/home.component';
import { PostSelectedComponent } from './views/post-selected/post-selected.component';

const routes: Routes = [

  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'post/:id',
    component: PostSelectedComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '**', redirectTo: '/'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
